from django.db import models


class TvShow(models.Model):
    TYPE_TV_SHOW = (
        ('Комедийные', 'Комедийные'),
        ('Научные', 'Научные'),
        ('Новости', 'Новости'),
        ('Хроника', 'Хроника'),
        ('Фантастика', 'Фантастика'),
        ('Мультфильмы', 'Мультфильмы')
    )
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='tv_show/', null=True)
    description = models.TextField()
    type_tv_show = models.CharField(max_length=100, choices=TYPE_TV_SHOW)
    url_tv_show = models.URLField(null=True)
    company_tv = models.CharField(max_length=35,null=True)
    age_restrictions = models.IntegerField()
    number_tv_show = models.IntegerField()
    instagram = models.URLField(null=True)
    about_tv_show = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Rating(models.Model):
    RATING = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
    )
    tv_show = models.ForeignKey(TvShow, on_delete=models.CASCADE,
                                related_name='comment_object')
    Text = models.CharField(max_length=350,null=True)
    Stars = models.CharField(max_length=100, choices=RATING)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.Stars


class Reviews(models.Model):
    tv_show_text = models.CharField('Комментарий:', max_length=500, null=True)
    rate_stars = ((i, '🌟' * i) for i in range(1, 6))
    rate = models.IntegerField('Оценка:', choices=rate_stars, null=True)
    created_at = models.DateField(auto_now_add=True, null=True)

    def __str__(self):
        return self.tv_show_text
