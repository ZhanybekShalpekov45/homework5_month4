from django import forms
from . import models
from tv_show.models import Rating


class Tv_Show_Form(forms.ModelForm):
    class Meta:
        model = models.TvShow
        fields = '__all__'


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = '__all__'
